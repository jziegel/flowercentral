<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/lightbox.css" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/header.js" type="text/javascript"></script>
<script src="js/prototype.js" type="text/javascript"></script>
<script src="js/scriptaculous.js?load=effects" type="text/javascript"></script>
<script src="js/lightbox.js" type="text/javascript"></script>
<script src="js/java.js" type="text/javascript"></script>