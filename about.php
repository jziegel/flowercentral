<?php include 'doctype.php'; ?>
<head>
<?php include 'meta.php'; ?>
<title>About us | Flower Shop</title>
</head>

<body>

<script type="text/javascript">
// Start jQuery in no-conflict mode
var $j = jQuery.noConflict();
$j(document).ready(function() {
  // Highlight menu and load products
	$j("#menu-about").addClass('selected');
	loadPromotedProducts(4, "#promoted_products");
});
</script>

<div id="wrap"> 
  <!-- Header -->
  <?php include 'header.php';?>
  <!-- End header --> 
  
  <!-- Center content -->
  <div class="center_content"> 
    
    <!-- Left content -->
    <div class="left_content">
      <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>About us</div>
      <div class="feat_prod_box_details">
        <p class="details"> <img src="images/about.jpg" alt="" title="" class="right" /> At Flower Central, we are passionate about bringing you the most beautiful, freshest blossoms to enrich your life with vibrant colours. We handpick the every item in our store to assure you the highest quality plants and flowers that are sure to delight you.</p>
        <p class="details">Based in Melbourne, Flower Central is your go-to for everything floral, no matter what your need might be. Whether it's a romantic gesture for an all important first date, or a bouquet to celebrate your special day, the Flower Shop has got you covered! </p>
      </div>
    </div>
    <!-- End left content --> 
    
    <!-- Right content -->
    <div class="right_content"> 
      
      <!-- My cart-->
      <?php include 'mycart.php' ?>
      <!-- End my cart --> 
      
      <!-- Promotions -->
      <div class="right_box">
        <div class="title"><span class="title_icon"> <img src="images/bullet4.gif" alt="" title="" /></span>Promotions </div>
        <div id="promoted_products"></div>
      </div>
      <!-- End promotions --> 
      
    </div>
    <!-- End right content-->
    
    <div class="clear"></div>
  </div>
  <!-- End center content--> 
  
  <!-- Footer -->
  <?php include 'footer.php'; ?>
  <!-- End Footer --> 
</div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    						  *
***************************************************************************************
-->