<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Contact | Flower Shop</title>
</head>

<body>

    <script type="text/javascript">
        // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu item and load products
            $j("#menu-contact").addClass('selected');
            loadPromotedProducts(4, "#promoted_products");
        });
    </script>
    
    <div id="wrap"> 
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Full content -->
        <div class="full_page"> 
            <div class="full_page_container">
                <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Contact</div>
                <div class="feat_prod_box_details">
                    <p class="details"> Got a question? Let us know. Our team is always on hand to help you out, no matter what your floral needs may be.</p>
                </div>

                <!-- Contact form -->  
                <form name="contact" class="contact_form" action="complete-contact.php" onsubmit="return validateContactForm();" method="post" >
                    <div class="form_subtitle">Please fill out the form below</div>
                    <p class="details">* Denotes mandatory fields.</p>
                    <div class="row">
                        <label class="contact"><strong>Name:<span class="required">*</span></strong></label>
                        <input type="text" name="name" id="name" autofocus="autofocus" class="contact_input" 
                               onblur="validateNotEmpty(this.value, '#nameError')"/>
                        <span id = "nameError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="contact"><strong>Email:<span class="required">*</span></strong></label>
                        <input type="text" name="email" id="email" class="contact_input" 
                               onblur="validateEmailFormat(this.value, '#emailError')"/>
                        <span id = "emailError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="contact"><strong>Phone:</strong></label>
                        <input type="text" name="phone" id="phone" class="contact_input" 
                               onblur="validatePhoneNonMandatory(this.value, '#phoneError')"/>
                        <span id = "phoneError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="contact"><strong>Company:</strong></label>
                        <input type="text" name="company" id="company" class="contact_input" />
                    </div>
                    <div class="row">
                        <label class="contact"><strong>Message:<span class="required">*</span></strong></label>
                        <textarea name="message" id="message" class="contact_textarea" 
                                  onblur="validateNotEmpty(this.value, '#messageError')" ></textarea>
                        <span id = "messageError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="contact"></label>
                            <img id="captcha" src="/sit203/securimage/securimage_show.php" alt="CAPTCHA Image" />
                            <a href="#" onclick="document.getElementById('captcha').src = '/sit203/securimage/securimage_show.php?' + Math.random(); return false">[ Change image ]</a>
                    </div>
                    <div class="row">
                        <label class="contact"><strong>Please enter the above characters:<span class="required">*</span></strong></label>
                        <input type="text" id="captcha_text" class="contact_input" name="captcha_code" size="10" maxlength="6" />
                        <span id="captchaError" class = "error"></span>
                    </div>
                    <div class="row">
                        <input type="submit" value="Send" />
                    </div>
                </form>
                <!-- End contact form -->

                <div class="clear"></div>
            </div>
            <!-- End full content -->

        <!-- Footer -->
        <?php include 'footer.php'; ?>
        <!-- End Footer --> 
        </div>
    </div>
</body>
</html>
<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    						  *
***************************************************************************************
-->