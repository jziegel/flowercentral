<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Register | Flower Shop</title>
</head>

<body>
    <script type="text/javascript">
    // Start jQuery in no-conflict mode;
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu
            $j("#menu-register").addClass('selected');
        });
    </script> 

    <div id="wrap">

        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Full content -->
        <div class="full_page">
            <div class="full_page_container">
                <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Register</div>
                <div class="feat_prod_box_details">
                    <p class="details"> Register with Flower Central today to receive exclusive deals and benefits.<br/>
                        You will also receive updates on our latest offer and exciting new arrivals in store.</p>
                </div>

                <!-- Registration form -->
                <form name="register" class="user_form" action="complete-register.php" onsubmit="return validateRegisterForm();" method="post" >
                    <div class="form_subtitle">Create new account</div>      
                    <div class="row">
                        <label class="user_form_title"><strong>Name:*</strong></label>
                        <input type="text" name="name" id="name" autofocus="autofocus" class="user_form_info"
                               onblur = "validateNotEmpty(this.value, '#nameError')"/>
                        <span id = "nameError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Password:*</strong></label>
                        <input type="password" name="password" id="password" class="user_form_info"
                               onblur = "validatePassword(this.value, '#passwordError')"/>
                        <span id = "passwordError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Email:*</strong></label>
                        <input type="text" name="email" id="email" class="user_form_info"
                               onblur = "validateEmail(this.value, '#emailError')"/>
                        <span id = "emailError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Phone number:*</strong></label>
                        <input type="text" name="phone" id="phone" class="user_form_info"
                               onblur = "validatePhone(this.value, '#phoneError')"/>
                        <span id = "phoneError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Unit no:</strong></label>
                        <input type="text" name="unit_no" id="unit_no" class="user_form_info_short"/>
                        <label class="user_form_title_short"><strong>Street:</strong></label>
                        <input type="text" name="street" id="street" class="user_form_info_short"/>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>City:</strong></label>
                        <input type="text" name="city" id="city" class="user_form_info_short"/>
                        <label class="user_form_title_short"><strong>Post code:</strong></label>
                        <input type="text" name="postcode" id="postcode" class="user_form_info_short"
                               onblur = "validatePostCode(this.value, '#postCodeError')"/>
                        <span id = "postCodeError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Cardholders name:</strong></label>
                        <input type="text" name="cardholder_name" id="cardholder_name" class="user_form_info"/>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Card type:</strong></label>
                        <select name="card_type">
                            <option value="visa">VISA</option>
                            <option value="mastercard">Master Card</option>
                            <option value="americanexpress">American Express</option>
                        </select>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Card number:</strong></label>
                        <input type="text" name="card_number" id="card_number" class="user_form_info"
                               onblur = "validateCardNumber(this.value, '#cardNumberError')"/>
                        <span id = "cardNumberError" class = "error"></span>
                    </div>
                    <div class="row">
                        <label class="user_form_title"><strong>Expiry:</strong></label>
                        <div class="expriy">
                            <input type="text" name="expiry_month" id="expiry_month"
                                   onblur = "validateExpiryDate(this.value, '#expiryDateError')"/>
                            /
                            <input type="text" name="expiry_year" id="expiry_year"
                                   onblur = "validateExpiryDate(this.value, '#expiryDateError')"/>
                        </div>
                        <span id = "expiryDateError" class = "error"></span>
                    </div>
                    <div class="row">
                        <div class="terms">
                            <input type="checkbox" name="terms" id="termsCheckbox"/>
                            I agree to the <a href="#">Terms &amp; Conditions</a>
                            <span id = "termsError" class = "error"></span>
                        </div>    
                    </div>
                    <div class="row">
                        <input type="submit" value="Register" />
                    </div>
                </form>
                <!-- End registration form -->

                <div class="clear"></div>
            </div>
            <!-- End full content--> 

            <!-- Footer -->
            <?php include 'footer.php'; ?>
            <!-- End Footer --> 
        </div>
    </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    			      *
***************************************************************************************
-->