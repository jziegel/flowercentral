<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Home | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
        // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu and load products
            $j("#menu-home").addClass('selected');
            loadFeaturedProducts(2, "#featured_products");
            loadNewProducts(4, "#new_products");
            loadPromotedProducts(4, "#promoted_products");
        });
    </script>

    <div id="wrap"> 
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Center content -->
        <div class="center_content"> 

            <!-- Left content -->
            <div class="left_content"> 

                <!-- Featured products -->
                <div class="title"> <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Featured products </div>
                <div id="featured_products"></div>
                <div class="clear"></div>
                <!-- End Featured products --> 

                <!-- New products -->
                <div class="title"> <span class="title_icon"><img src="images/bullet2.gif" alt="" title="" /></span>New products </div>
                <div class="clear"></div>
                <div id="new_products"></div>
                <div class="clear"></div>
                <!-- End new products --> 

            </div>
            <!-- End left content--> 

            <!-- Right content -->
            <div class="right_content"> 

                <!-- My cart-->
                <?php include 'mycart.php' ?>
                <!-- End my cart --> 

                <!-- About -->
                <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>About us</div>
                <div class="about">
                    <p> <img src="images/about.jpg" alt="" title="" class="right" /> 
                        <!-- Barni1 (N.D.), Flower shop bouquet, pixabay.com, retrieved 16 August 2016<https://pixabay.com/en/flowers-flower-shop-bouquet-658514/>. --> 
                        Flower Central is your one stop flower shop, with the world's best and most beautiful flowers and floral gifts under one roof. We stock a wide variety of traditional and exotic flowers suited to a range of tastes, and deliver across Melbourne, even on weekends!</p>
                </div>
                <!-- End about --> 

                <!-- Promotions -->
                <div class="right_box">
                    <div class="title"><span class="title_icon"> <img src="images/bullet4.gif" alt="" title="" /></span>Promotions</div>
                    <div id="promoted_products"></div>
                </div>
                <!-- End promotions -->

            </div>
            <!-- End right content-->

            <div class="clear"></div>
        </div>
        <!-- End center content--> 

        <!-- Footer -->
        <?php include 'footer.php'; ?>
        <!-- End Footer --> 
    </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    			      *
***************************************************************************************
-->