<?php

$q=$_GET["q"];

require_once('inc/global-connect.inc.php');

// SQL statement to match entries that begin with the query in both Plants and Gifts tables
$query ="SELECT ID, name
		 FROM Plants
		 WHERE UPPER(name) LIKE UPPER('" . $q . "%')
		 UNION
		 SELECT ID, name
		 FROM Gifts
		 WHERE UPPER(name) LIKE UPPER('" . $q . "%')";

// Check statement for errors
$stmt = oci_parse($connect, $query);

// Display an error if there is one
if(!$stmt) {
	echo "Search error\n";
	exit;
}

oci_execute($stmt);

// Display a list of matching names and anchor it to the details page
while(oci_fetch_array($stmt)) {
	$id =  oci_result($stmt,"ID");
	$name = oci_result($stmt,"NAME");
	echo "<a href=details.php?id=" . $id . ">" . $name . "</a>";
}

// Close the connection
oci_close($connect); 
?>