<div class="header">
    <div>
        <div class="logo"><a href="index.php"><img src="images/logo.gif" alt="" title="" border="0" /></a></div>
        <?php
        if (isset($_COOKIE["fc_username"])) {
            echo '<div id="userInfo" class="logged_in_info"><a href="myaccount.php">Welcome ' . $_COOKIE['fc_username'] . '</a></div>';
        }
        ?>
    </div>
    <div id="menu">
        <ul>
            <li id="menu-home"><a href="index.php">Home</a></li>
            <li id="menu-about"><a href="about.php">About</a></li>
            <li id="menu-plants"><a href="plants.php">Plants</a></li>
            <li id="menu-gifts"><a href="gifts.php">Gifts</a></li>
            <li id="menu-myaccount"><a href="myaccount.php">Account</a></li>
            <!-- Hide the Register menu item if user is already logged in -->
            <?php
            if (!isset($_SESSION['fc_username']) && !isset($_COOKIE["fc_username"])) {
                echo '<li id="menu-register"><a href="register.php">Register</a></li>';
            }
            ?>
            <li id="menu-contact"><a href="contact.php">Contact</a></li>
        </ul>
        <div class="header_search_form">
            <input type="text" name="search" autofocus id="search_box" onkeyup="showItem(this.value)" />
            <input type="submit" name="submit" id="submit" value="Search" class="contact"/>
            <div id="dropdown" class="dropdown-content"></div>
        </div>
    </div>
</div>
