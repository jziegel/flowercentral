<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Register | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
    // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu
            $j("#menu-register").addClass('selected');
        });
    </script>

    <div id="wrap">

        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Full content -->
        <div class="full_page">
            <div class="full_page_container">
                <?php
                require_once('inc/global-connect.inc.php');
                require_once('inc/functions.inc.php');

                $name = test_input($_POST['name']);
                $password = test_input($_POST['password']);
                $email = test_input($_POST['email']);
                $phone = test_input($_POST['phone']);
                $unit_no = test_input($_POST['unit_no']);
                $street = test_input($_POST['street']);
                $city = test_input($_POST['city']);
                $postcode = test_input($_POST['postcode']);
                $cardholder_name = test_input($_POST['cardholder_name']);
                $card_type = test_input($_POST['card_type']);
                $card_number = test_input($_POST['card_number']);
                $expiry_month = test_input($_POST['expiry_month']);
                $expiry_year = test_input($_POST['expiry_year']);
                
                $salt = "vbOKD64lPJ8QoWrukTJmofJdw8y2nuFakOgXpyrS1V";
                
                global $salt;
                
                // Hash password
                $password = md5($salt.$password);
                

                $sql = "INSERT INTO Users 
			VALUES ('" . $email . "','" . $name . "', '" . $password . "', '"
                        . $phone . "','" . $unit_no . "', '" . $street . "', '"
                        . $city . "','" . $postcode . "', '" . $cardholder_name . "', '"
                        . $card_type . "','" . $card_number . "', '" . $expiry_month . "', '"
                        . $expiry_year . "')";


                $stmt = oci_parse($connect, $sql);
                if (!$stmt) {
                    echo '  <div class="title">
                                <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
                            </div>
                            <div class="feat_prod_box_details">
      				<p class="details">Error in preparing query statement. Please go back and try again.</p>
                            </div>';
                    exit;
                }

                oci_execute($stmt);
                echo '<div class="title">
				<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Registration successful
			</div>
			<div class="feat_prod_box_details">
				<p class="details">Your details below have been successfully registered with the system: <br/>
								   	Name: ' . $name . '<br/>Email: ' . $email . '<br/>Phone: ' . $phone . '<br/>
				</p>
			</div>
			<div class="clear"></div>';
                echo "<button type='button' class='register_button' onclick='window.location=\"myaccount.php\"'>
			Login to your account</button>";
                oci_commit($connect);
                oci_close($connect);
                ?>

                <div class="clear"></div>
            </div>
            <!-- End full content--> 

            <!-- Footer -->
            <?php include 'footer.php'; ?>
            <!-- End Footer --> 
        </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    			      *
***************************************************************************************
-->