<?php

$q = $_GET["q"];

require_once('inc/global-connect.inc.php');

// SQL statement to match entries that begin with the query in both Plants and Gifts tables
$query = "SELECT email
		 FROM Users";

// Check statement for errors
$stmt = oci_parse($connect, $query);

// Display an error if there is one
if (!$stmt) {
    echo false;
    exit;
}

oci_execute($stmt);

$exists = false;

// Display a list of matching names and anchor it to the details page
while (oci_fetch_array($stmt)) {
    if (oci_result($stmt, "EMAIL") == $q) {
        $exists = true;
        break;
    } else {
        $exists = false;
    }
}

echo $exists;

// Close the connection
oci_close($connect);
?>