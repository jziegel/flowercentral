<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Cart | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
    // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
        });
    </script>

    <div id="wrap"> 
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header -->

        <?php
        require_once('inc/global-connect.inc.php');
        require_once('inc/functions.inc.php');

        // Get cart session
        $cart = $_SESSION['fc_cart'];

        // Check if a get action is available
        if (isset($_GET['action'])) {
            $action = $_GET['action'];

            // Process action
            switch ($action) {
                case 'add':
                    if ($cart) {
                        $cart .= ',' . $_GET['id'];
                    } else {
                        $cart = $_GET['id'];
                    }
                    break;
                case 'delete':
                    if ($cart) {
                        $items = explode(',', $cart);
                        $newcart = '';
                        foreach ($items as $item) {
                            if ($_GET['id'] != $item) {
                                if ($newcart != '') {
                                    $newcart .= ',' . $item;
                                } else {
                                    $newcart = $item;
                                }
                            }
                        }
                        $cart = $newcart;
                    }
                    break;
                case 'update':
                    if ($cart) {
                        $newcart = '';
                        foreach ($_POST as $key => $value) {
                            if (stristr($key, 'qty')) {
                                $id = str_replace('qty', '', $key);
                                $items = ($newcart != '') ? explode(',', $newcart) : explode(',', $cart);
                                $newcart = '';
                                foreach ($items as $item) {
                                    if ($id != $item) {
                                        if ($newcart != '') {
                                            $newcart .= ',' . $item;
                                        } else {
                                            $newcart = $item;
                                        }
                                    }
                                }
                                for ($i = 1; $i <= $value; $i++) {
                                    if ($newcart != '') {
                                        $newcart .= ',' . $id;
                                    } else {
                                        $newcart = $id;
                                    }
                                }
                            }
                        }
                    }
                    $cart = $newcart;
                    break;
            }
            $_SESSION['fc_cart'] = $cart;
        }
        ?>

        <!-- Full page content -->
        <div class="full_page">
            <div class="full_page_container">
                <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>My cart</div>
                <div class="feat_prod_box_details">
                    <p class="details"><?php echo writeShoppingCart(); ?></p>
                    <?php echo showCart(); ?> </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- End full content--> 

        <!-- Footer -->
        <?php include 'footer.php'; ?>
        <!-- End Footer --> 
    </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                                              *
***************************************************************************************
-->