<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Gifts | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
        // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu and load products
            $j("#menu-gifts").addClass('selected');
            loadPromotedProducts(4, "#promoted_products");
        });
    </script>

    <div id="wrap"> 
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Center content -->
        <div class="center_content"> 

            <!-- Left content -->
            <div class="left_content">
                <div class="title"> <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Gift catalogue </div>
                <?php
                require_once('inc/global-connect.inc.php');

                // Find new products
                $query = "SELECT ID
				 FROM Gifts";

                // Run query
                $stmt = oci_parse($connect, $query);

                if (!$stmt) {
                    echo "An error occurred in parsing the sql string.\n";
                    exit;
                }
                oci_execute($stmt);
                $gifts = array();

                while (oci_fetch_array($stmt)) {
                    $gifts[] = oci_result($stmt, "ID");
                }

                $gifts_count = count($gifts);

                for ($x = 0; $x < $gifts_count; $x++) {

                    $new_query = "SELECT *
						 FROM Gifts
						 WHERE ID = '" . $gifts[$x] . "'";
                    $stmt = oci_parse($connect, $new_query);
                    if (!$stmt) {
                        echo "An error occurred in parsing the sql string.\n";
                        exit;
                    }
                    oci_execute($stmt);
                    while (oci_fetch_array($stmt)) {
                        $id = oci_result($stmt, "ID");
                        $name = oci_result($stmt, "NAME");
                        $description = oci_result($stmt, "DESCRIPTION");
                        $link = oci_result($stmt, "LINK");
                        echo '<div class="feat_prod_box">
							<div class="prod_img">
								<a href="details.php?id=' . $id . '"><img src="images/products/' . $link . '.jpg" alt="" title="" border="0"></a>
							</div>
							<div class="prod_det_box">
								<div class="box_top"></div>
								<div class="box_center">
									<div class="prod_title">' . $name . '</div>
									<p class="details">' . $description . '</p>
									<a href="details.php?id=' . $id . '" class="more">- more details -</a>
									<div class="clear"></div>
								</div>
								<div class="box_bottom"></div>
							</div>
							<div class="clear"></div>
					   </div>';
                    }
                }
                // Close the connection
                oci_close($connect);
                ?>
                <div class="clear"></div>
            </div>
            <!--end of left content--> 

            <!-- Right content -->
            <div class="right_content"> 

                <!-- My cart-->
                <?php include 'mycart.php' ?>
                <!-- End my cart --> 

                <!-- About -->
                <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>About us</div>
                <div class="about">
                    <p> <img src="images/about.jpg" alt="" title="" class="right" /> 
                        <!-- Barni1 (N.D.), Flower shop bouquet, pixabay.com, retrieved 16 August 2016<https://pixabay.com/en/flowers-flower-shop-bouquet-658514/>. --> 
                        Flower Central is your one stop flower shop, with the world's best and most beautiful flowers and floral gifts under one roof. We stock a wide variety of traditional and exotic flowers suited to a range of tastes, and deliver across Melbourne, even on weekends!</p>
                </div>
                <!-- End about --> 

                <!-- Promotions -->
                <div class="right_box">
                    <div class="title"><span class="title_icon"> <img src="images/bullet4.gif" alt="" title="" /></span>Promotions </div>
                    <div id="promoted_products"></div>
                </div>
                <!-- End promotions --> 

            </div>
            <!-- End right content-->

            <div class="clear"></div>
        </div>
        <!-- End center content--> 

        <!-- Footer -->
        <?php include 'footer.php'; ?>
        <!-- End Footer --> 
    </div>
</body>
</html>
<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    			      *
***************************************************************************************
-->