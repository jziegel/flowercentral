<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Flowers | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
    // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highloght menu item and load products
            $j("#menu-plants").addClass('selected');
            loadPromotedProducts(2, "#promoted_products");
            loadNewProducts(3, "#new_products");
            var tabber1 = new Yetii({
                id: 'demo'
            });
        });
    </script>
    <div id="wrap"> 
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Center content -->
        <div class="center_content"> 

            <!-- Left content -->
            <?php
            require_once('inc/global-connect.inc.php');

            if (!isset($_GET['id'])) {
                print '<p class="details">Please revisit this page through Gifts or Plants pages.</p>';
                exit;
            } else {
                $id = $_GET["id"];

                // Create a new query based on the product type
                $product_type = substr($id, 0, 1);
                $new_query;
                switch ($product_type) {
                    case 1:
                        // Product is a Plant
                        $new_query = "SELECT *
						 FROM Plants
						 WHERE ID = '" . $id . "'";

                        // Run query
                        $stmt = oci_parse($connect, $new_query);
                        if (!$stmt) {
                            echo "An error occurred in parsing the sql string.\n";
                            exit;
                        }
                        oci_execute($stmt);

                        // Output details
                        while (oci_fetch_array($stmt)) {
                            $name = oci_result($stmt, "NAME");
                            $description = oci_result($stmt, "DESCRIPTION");
                            $price = oci_result($stmt, "PRICE");
                            $link = oci_result($stmt, "LINK");
                            $botanical = oci_result($stmt, "BOTANICAL_NAME");
                            $season = oci_result($stmt, "SEASON");
                            echo '	<div class="left_content">
							<div class="crumb_nav"> <a href="index.php">Home</a> &gt;&gt; ' . $name . '</div>
							<div class="title">
								<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>' . $name . '
							</div>
							<div class="feat_prod_box_details">
								<div class="prod_img"><img src="images/products/' . $link . '_thumb.jpg" /><br /><br />
									<a href="images/products/' . $link . '_large.jpg" rel="lightbox">
										<img src="images/zoom.gif"/>
									</a> 				
								</div>
								<div class="prod_det_box">
									<div class="box_top"></div>
									<div class="box_center">
										<div class="prod_title">Details</div>
										<p class="details">' . $description . '</p>
										<div class="price"><strong>PRICE:</strong> <span class="red">' . $price . '</span></div>
										<button class="add_to_cart_button more" onclick="window.location.href=\'cart.php?action=add&id=' . $id . ' \';">Add to cart</button>
										<div class="clear"></div>
									</div>
									<div class="box_bottom"></div>
								</div>
								<div class="clear"></div>
							</div>';
                            echo '	<div id="demo" class="demolayout">
								<ul id="demo-nav" class="demolayout">
									<li><a class="active" href="#tab1">More details</a></li>
									<li><a class="" href="#tab2">Related Products</a></li>
								</ul>
								<div class="tabs-container">
									<div style="display: block;" class="tab" id="tab1">
										<p class="more_details">Additional details on ' . $name . 's:</p>
										<ul class="list">
											<li>Botanical name: ' . $botanical . ' </li>
											<li>Season: ' . $season . '</li>
										</ul>
									</div>
									<div style="display: none;" class="tab" id="tab2">
										<div id="new_products"></div>	
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
    				</div>';
                        }
                        break;
                    case 2:
                        // Product is a Gift
                        $new_query = "SELECT *
						 FROM Gifts
						 WHERE ID = '" . $id . "'";

                        // Run query
                        $stmt = oci_parse($connect, $new_query);
                        if (!$stmt) {
                            echo "An error occurred in parsing the sql string.\n";
                            exit;
                        }
                        oci_execute($stmt);

                        // Output details
                        while (oci_fetch_array($stmt)) {
                            $name = oci_result($stmt, "NAME");
                            $description = oci_result($stmt, "DESCRIPTION");
                            $price = oci_result($stmt, "PRICE");
                            $link = oci_result($stmt, "LINK");
                            $birthday = oci_result($stmt, "BIRTHDAY");
                            $wedding = oci_result($stmt, "WEDDING");
                            $anniversary = oci_result($stmt, "ANNIVERSARY");
                            $valentines = oci_result($stmt, "VALENTINES");
                            $dates = oci_result($stmt, "DATES");
                            $memorial = oci_result($stmt, "MEMORIAL");

                            echo '	<div class="left_content">
							<div class="crumb_nav"> <a href="index.php">Home</a> &gt;&gt; ' . $name . '</div>
							<div class="title">
								<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>' . $name . '
							</div>
							<div class="feat_prod_box_details">
								<div class="prod_img"><img src="images/products/' . $link . '_thumb.jpg" /><br /><br />
									<a href="images/products/' . $link . '_large.jpg" rel="lightbox">
										<img src="images/zoom.gif"/>
									</a> 				
								</div>
								<div class="prod_det_box">
									<div class="box_top"></div>
									<div class="box_center">
										<div class="prod_title">Details</div>
										<p class="details">' . $description . '</p>
										<div class="price"><strong>PRICE:</strong> <span class="red">' . $price . '</span></div>
										<button class="add_to_cart_button more" onclick="window.location.href=\'cart.php?action=add&id=' . $id . ' \';">Add to cart</button>
										<div class="clear"></div>
									</div>
									<div class="box_bottom"></div>
								</div>
								<div class="clear"></div>
							</div>';
                            echo '	<div id="demo" class="demolayout">
								<ul id="demo-nav" class="demolayout">
									<li><a class="active" href="#tab1">More details</a></li>
									<li><a class="" href="#tab2">Related Products</a></li>
								</ul>
								<div class="tabs-container">
									<div style="display: block;" class="tab" id="tab1">
										<p class="more_details">' . $name . ' is a suitable gift for:</p>
										<ul class="list">
										 	' . (($birthday == 't') ? '<li>Birthdays</li>' : '') . '
											' . (($wedding == 't') ? '<li>Wedding</li>' : '') . '
											' . (($anniversary == 't') ? '<li>Anniversary</li>' : '') . '
											' . (($valentines == 't') ? '<li>Valentines</li>' : '') . '
											' . (($dates == 't') ? '<li>Dates</li>' : '') . '
											' . (($memorial == 't') ? '<li>Memorial</li>' : '') . '
										</ul>
									</div>
									<div style="display: none;" class="tab" id="tab2">
										<div id="new_products"></div>	
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
    				</div>';
                        }
                        break;
                }
            }

// Close the connection
            oci_close($connect);
            ?>

            <!-- End left content--> 

            <!-- Right content -->
            <div class="right_content"> 

                <!-- My cart-->
                <?php include 'mycart.php' ?>
                <!-- End my cart --> 

                <!-- About -->
                <div class="about">
                    <p> <img src="images/about.jpg" alt="" title="" class="right" /> 
                        <!-- Barni1 (N.D.), Flower shop bouquet, pixabay.com, retrieved 16 August 2016<https://pixabay.com/en/flowers-flower-shop-bouquet-658514/>. --> 
                        Flower Central is your one stop flower shop, with the world's best and most beautiful flowers and floral gifts under one roof. We stock a wide variety of traditional and exotic flowers suited to a range of tastes, and deliver across Melbourne, even on weekends!</p>
                </div>
                <!-- End about --> 

                <!-- Promotions -->
                <div class="right_box">
                    <div class="title"><span class="title_icon"> <img src="images/bullet4.gif" alt="" title="" /></span>Promotions </div>
                    <div id="promoted_products"></div>
                </div>
                <!-- End promotions --> 

            </div>
            <!-- End right content-->

            <div class="clear"></div>
        </div>
        <!-- End center content--> 

        <!-- Footer -->
        <?php include 'footer.php'; ?>
        <!-- End Footer --> 
    </div>
</body>
</html>
<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    						  *
***************************************************************************************
-->