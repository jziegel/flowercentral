<?php
// Variables to keep track of logged in status, errors and entered variables
$loggedIn = false;
$loginError = false;
$name = $email = "";

require_once('inc/global-connect.inc.php');
require_once('inc/functions.inc.php');

// Start processing the request if there is one
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $salt = "vbOKD64lPJ8QoWrukTJmofJdw8y2nuFakOgXpyrS1V";
    global $salt;

    // Get the email address and password
    $email = test_input($_POST['email']);
    $password = test_input($_POST['password']);
    
    $password =  md5($salt.$password);

    // Query to find a matching user
    $sql = "SELECT *
          FROM Users
          WHERE email = '$email'
          AND password = '$password'";

    $stmt = oci_parse($connect, $sql);

    // Display an error if there is a problem with the statement
    if (!$stmt) {
        echo '<div class="title">
            <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
          </div>
          <div class="feat_prod_box_details">
              <p class="details">Error in preparing query statement. Please go back and try again.</p>
          </div>';
        exit;
    }

    // Execute the statement
    oci_execute($stmt);

    // Variable used to figure out if the user was found
    $userCount = 0;

    while (oci_fetch_array($stmt)) {
        // If the while loop executes, that means a matching user has been found as the loop will not execute of oci_fetch_array($stmt) is null
        $userCount++;

        // Get the user details
        $username = oci_result($stmt, "NAME");
        $useremail = oci_result($stmt, "EMAIL");
        $userpassword = oci_result($stmt, "PASSWORD");
        $userphone = oci_result($stmt, "PHONE");
        $loggedIn = true;

        session_start();

        // Set sessions and cookies
        $_SESSION['fc_username'] = $username;
        $_SESSION['fc_useremail'] = $useremail;
        $_SESSION['fc_cart'] = '';
        setcookie("fc_username", $username, time() + 3600, '/');
        setcookie("fc_useremail", $useremail, time() + 3600, '/');
    }

    // If the count hasn't increased, that means oci_fetch_array($stmt) has returned null, so there is a login error
    // So an error message should be displayed when the page loads, and db connections closed
    if ($userCount == 0) {
        $loginError = true;
        oci_commit($connect);
        oci_close($connect);
    }
}


// If sessions or cookies are set, that means the user is logged in
if (isset($_SESSION['fc_username'])) {
    $loggedIn = true;
}
if (isset($_COOKIE["fc_username"])) {
    $loggedIn = true;
}

// Start displaying the page
include 'doctype.php';
echo '<head>';
include 'meta.php';
echo '<title>My Account | Flower Shop</title>
      </head>
      <body>
      <script type="text/javascript">

      // Start jQuery in no-conflict mode
      var $j = jQuery.noConflict();
      $j(document).ready(function() {
        // Highlight the current menu item
        $j("#menu-myaccount").addClass(\'selected\');
      });
      </script>

      <div id="wrap">';

// Content
echo '<div class="full_page">';
include 'header.php';

// If the user is not logged in, that means its either the first time the user is visting the page or theere has been a login error
if ($loggedIn == false) {

    // Login section
    echo '<div class="full_page_container" id="login_section">
          <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>My account</div>
          <div class="feat_prod_box_details">
            <p class="details"> Login to your account to manage orders, view transactions and more...</p>
        </div>';

    // Login form
    echo '<form name="login" class="user_form" action="';
    echo htmlentities($_SERVER['PHP_SELF']);
    echo '" method="post">
          <div class="form_subtitle">Login into your account</div>
          <div class="row">
            <label class="user_form_title"><strong>Email:</strong></label>
            <input class="user_form_info" name="email" type="text" ';
    // If the email has been sent to the page, display it so the user doesn't have to type it again
    if ($email != "") {
        echo 'value="' . $email . '"';
    }
    echo 'onblur = "validateEmailLogin(this.value, \'#emailError\')"/>';
    echo '<span id = "emailError" class = "error"></span>
          </div>
          <div class="row">
            <label class="user_form_title"><strong>Password:</strong></label>
            <input class="user_form_info" name="password" type="password" class="contact_input" />
            <span id = "passwordError" class = "error" ';
    // If there is a login error, display an error message
    if ($loginError === false) {
        echo 'style="display:none"';
    }
    echo '>Invalid password entered</span>
          </div>
          <div class="row">
            <input type="submit" class="register" value="Login" />
          </div>
        </form>';
    // End login form
    // End login section
} else {
    // My account section

    if (isset($_SESSION['fc_username'])) {
        $username = $_SESSION['fc_username'];
        $useremail = $_SESSION['fc_useremail'];
    }
    if (isset($_COOKIE["fc_username"])) {
        $username = $_COOKIE['fc_username'];
        $useremail = $_COOKIE['fc_useremail'];
    }

    // Query to find a matching user
    $query = "SELECT *
            FROM Users
            WHERE email = '$useremail'";

    $stmt = oci_parse($connect, $query);

    // Display an error if there is a problem with the statement
    if (!$stmt) {
        echo '<div class="title">
            <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
          </div>
          <div class="feat_prod_box_details">
              <p class="details">Error in preparing query statement. Please go back and try again.</p>
          </div>';
        exit;
    }

    // Execute the statement
    oci_execute($stmt);

    while (oci_fetch_array($stmt)) {
        // Get the user details
        $userphone = oci_result($stmt, "PHONE");
    }

    // If the user has susccessfully logged in, display the user details
    echo '<div class="full_page_container" id="my_account_section">
          <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>My account</div>
          <div class="feat_prod_box_details">
            <p class="details">View your account details and past orders from this section.</p>
            <p class="details">Your details: <br/>
                                Name: ' . $username . '<br/> 
                                Email: ' . $useremail . '<br/>
                                Phone: ' . $userphone . '<br/>
          </div>';
    echo showMyOrders();
    echo '<div class="clear"></div>
      </div>';

    oci_commit($connect);
    oci_close($connect);

    // End my account section
}

echo '<div class="clear"></div>
      </div>';

include 'footer.php';

echo '</div>
      </body>
      </html>';
?>
<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    						  *
***************************************************************************************
-->