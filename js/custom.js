// Functions to load featured, new and promoted products into the given div

var $j = jQuery.noConflict();

function loadFeaturedProducts(num, divId) {
    var xmlhttpFeaturedProduct;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttpFeaturedProduct = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttpFeaturedProduct = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttpFeaturedProduct.onreadystatechange = function () {
        if (xmlhttpFeaturedProduct.readyState === 4 && xmlhttpFeaturedProduct.status === 200) {
            $j(divId).html(xmlhttpFeaturedProduct.responseText);
        }
    };
    xmlhttpFeaturedProduct.open("GET", "featured-products.php?q=" + num, true);
    xmlhttpFeaturedProduct.send();
}
function loadNewProducts(num, divId) {
    var xmlhttpNewProduct;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttpNewProduct = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttpNewProduct = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttpNewProduct.onreadystatechange = function () {
        if (xmlhttpNewProduct.readyState === 4 && xmlhttpNewProduct.status === 200) {
            $j(divId).html(xmlhttpNewProduct.responseText);
        }
    };
    xmlhttpNewProduct.open("GET", "new-products.php?q=" + num, true);
    xmlhttpNewProduct.send();
}
function loadPromotedProducts(num, divId) {
    var xmlhttpPromotedProduct;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttpPromotedProduct = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttpPromotedProduct = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttpPromotedProduct.onreadystatechange = function () {
        if (xmlhttpPromotedProduct.readyState === 4 && xmlhttpPromotedProduct.status === 200) {
            $j(divId).html(xmlhttpPromotedProduct.responseText);
        }
    };
    xmlhttpPromotedProduct.open("GET", "promoted-products.php?q=" + num, true);
    xmlhttpPromotedProduct.send();
}


// Functions for register form

// Function to validate password length and numeric requirement
function validatePassword(input, errorDisplayId) {
    var valid = false;
    if (validateNotEmpty(input, errorDisplayId)) {
        if (input.length >= 6) {
            var regEx = /[0-9]/;
            if (regEx.test(input)) {
                $j(errorDisplayId).text("");
                console.log("Password is valid");
                valid = true;
            } else {
                $j(errorDisplayId).text("Password must contain at least 1 number");
                console.log("Password is invalid");
                valid = false;
            }
        } else {
            $j(errorDisplayId).text("Password must contain at least 6 characters");
            console.log("Password is invalid");
            valid = false;
        }
    } else {
        valid = false;
    }
    return valid;
}

// Function to validate phone number numeric requirement
function validatePhone(input, errorDisplayId) {
    var valid = false;
    if (validateNotEmpty(input, errorDisplayId)) {
        if (input.length >= 10 && validateNumbersOnly(input)) {
            $j(errorDisplayId).text("");
            valid = true;
        } else {
            $j(errorDisplayId).text("Phone number should consit of over 9 digits");
            valid = false;
        }
    } else {
        valid = false;
    }
    return valid;
}

// Function to validate non-mandatory phone number numeric requirement
function validatePhoneNonMandatory(input, errorDisplayId) {
    var valid = false;
    if (validateNotEmpty(input, errorDisplayId)) {
        if (input.length >= 10 && validateNumbersOnly(input)) {
            $j(errorDisplayId).text("");
            valid = true;
        } else {
            $j(errorDisplayId).text("Phone number should consit of over 9 digits");
            valid = false;
        }
    } else {
        $j(errorDisplayId).text("");
        valid = false;
    }
    return valid;
}

// Function to validate email
function validateEmail(input, errorDisplayId) {
    var valid = false;
    if (validateNotEmpty(input, errorDisplayId)) {
        console.log("Email is not empty");
        var xmlhttp;
        if (validateEmailFormat(input, errorDisplayId)) {
            try {
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                        var result = xmlhttp.responseText;
                        console.log("Email query result is: " + result);
                        if (result === 1) {
                            $j(errorDisplayId).text("Email already exists in system, did you mean to log in?");
                            console.log("Email exists in system");
                            valid = false;
                            return;
                        } else {
                            $j(errorDisplayId).text("");
                            console.log("Email doesn't exist in system");
                            valid = true;
                            return;
                        }
                    }
                };
                // Not sending the request async because we need to get a response before moving on
                xmlhttp.open("GET", "check-email.php?q=" + input, false);
                xmlhttp.send();
            } catch (exception) {
                alert('Request Failed');
                console.log("Email verification request failed");
                return;
            }
        } else {
            $j("#emailError").text("Email format is invalid");
            valid = false;
            return;
        }
    } else {
        console.log("Email is empty");
        valid = false;
        return;
    } 
    return valid;
}

// Function to validate car number length and numeric requirement
function validateCardNumber(input, errorDisplayId) {
    var valid = false;
    if (input.length === 16 && validateNumbersOnly(input)) {
        $j(errorDisplayId).text("");
        valid = true;
    } else {
        $j(errorDisplayId).text("Credit card muct be exactly 16 digits");
        valid = false;
    }
    return valid;
}

// Function to validate email field
function validateEmailFormat(input, errorDisplayId) {
    var valid = false;
    var section = 'user';
    for (var i = 0; i < input.length && !valid; i++) {
        if (section === "user" && input.charAt(i) === '@') {
            section = "domain";
        } else if (section === "domain" && input.charAt(i) === '.') {
            if (i !== input.length - 1) {
                valid = true;
            }
        }
    }
    if (!valid) {
        $j(errorDisplayId).text("E-mail has incorrect format");
        console.log("Email format is invalid");
    } else {
        $j(errorDisplayId).text("");
        console.log("Email format is valid");
    }

    return valid;
}

// Function to validate whether a given field is empty
function validateNotEmpty(input, errorDisplayId) {
    var notEmpty = false;
    if (input === "" || input === null) {
        $j(errorDisplayId).text("This is a required field");
        notEmpty = false;
    } else {
        $j(errorDisplayId).text("");
        notEmpty = true;
    }
    return notEmpty;
}

// Function to validate if a given field contains numbers only
function validateNumbersOnly(input) {
    var valid = false;
    var regEx = /^\d+$/;
    if (regEx.test(input)) {
        valid = true;
    } else {
        valid = false;
    }
    return valid;
}

// Function to vaidate date field numeric and length requirement
function validateExpiryDate(input, errorDisplayId) {
    var valid = false;
    if (input.length === 2 && validateNumbersOnly(input)) {
        $j(errorDisplayId).text("");
        valid = true;
    } else {
        $j(errorDisplayId).text("Expiry date shoud be in format MM-YY");
        valid = false;
    }
    return valid;
}

// Function to vaidate security code field numeric and length requirement
function validateSecurityCode(input, errorDisplayId) {
    var valid = false;
    if (input.length === 3 && validateNumbersOnly(input)) {
        $j(errorDisplayId).text("");
        valid = true;
    } else {
        $j(errorDisplayId).text("Security code should be 3 digits");
        valid = false;
    }
    return valid;
}

// Function to vaidate postcode field numeric and length requirement
function validatePostCode(input, errorDisplayId) {
    var valid = false;
    if (input.length === 4 && validateNumbersOnly(input)) {
        $j(errorDisplayId).text("");
        valid = true;
    } else {
        $j(errorDisplayId).text("Post code should be 4 digits");
        valid = false;
    }
    return valid;
}

// Function to validate a checkbox status
function termsAgreed() {
    var checked = false;
    if ($j('#termsCheckbox').is(':checked')) {
        checked = true;
        $j('#termsError').html("");
        console.log("Terms checked");
    } else {
        checked = false;
        $j('#termsError').html("Please agree to the Terms & Conditions");
        console.log("Terms not checked");
    }
    return checked;
}


// Function to validate the register form
function validateRegisterForm() {
    // Track the validity of the name, email, password, phone number and terms checkbox
    var nameValid = false;
    var emailValid = false;
    var passwordValid = false;
    var phoneValid = false;
    var termsChecked = false;
    
    // Find out if the name, email, password, phone number if the terms checkbox has been checked
    nameValid = validateNotEmpty($j('#name').val(), $j('#nameError'));
    emailValid = validateEmail($j('#email').val(), $j('#emailError'));
    passwordValid = validatePassword($j('#password').val(), $j('#passwordError'));
    phoneValid = validatePhone($j('#phone').val(), $j('#phoneError'));
    termsChecked = termsAgreed();
    
    // Stop the form from progressing if the requirements are not met
    if (nameValid && emailValid && passwordValid && phoneValid && termsChecked) {
        console.log("validateRegisterForm returned true");
    } else {
        console.log("validateRegisterForm returned false");
        if(event.preventDefault){
            event.preventDefault();
        }else{
            event.returnValue = false; // for IE
        }
    }
    
    return (nameValid && emailValid && passwordValid && phoneValid && termsChecked);
}

// Function to validate the register form
function validateContactForm() {
    // Track name, email and message    
    var nameValid = false;
    var emailValid = false;
    var messageValid = false;
    var captchaValid = false;
    
    // find out if name, email and message are valid
    nameValid = validateNotEmpty($j('#name').val(), $j('#nameError'));
    if (validateNotEmpty($j('#email').val(), $j('#emailError')))
        emailValid = validateEmailFormat($j('#email').val(), $j('#emailError'));
    messageValid = validateNotEmpty($j('#message').val(), $j('#messageError'));
    messageValid = validateNotEmpty($j('#captcha_text').val(), $j('#captchaError'));

    // Stop the form from progressing if the requirements are not met
    if (nameValid && emailValid && messageValid) {
        console.log("validateContactForm returned true");
    } else {
        console.log("validateContactForm returned false");
        if(event.preventDefault){
            event.preventDefault();
        }else{
            event.returnValue = false; // for IE
        }
    }
    
    return (nameValid && emailValid && messageValid);
}

// Function to validate the order form
function validateOrderForm() {
    var nameValid = validateNotEmpty($j('#name').val(), $j('#nameError'));
    var phoneValid = validatePhone($j('#phone').val(), $j('#phoneError'));
    var streetValid = validateNotEmpty($j('#street').val(), $j('#streetError'));
    var cityValid = validateNotEmpty($j('#city').val(), $j('#cityError'));
    var postcodeValid = validatePostCode($j('#postcode').val(), $j('#postCodeError'));
    var cardholderNameValid = validateNotEmpty($j('#cardholder_name').val(), $j('#cardholderNameError'));
    var cardNumberValid = validateCardNumber($j('#card_number').val(), $j('#cardNumberError'));
    var expiryMonthValid = validateExpiryDate($j('#expiry_month').val(), $j('#expiryDateError'));
    var expiryYearValid = validateExpiryDate($j('#expiry_year').val(), $j('#expiryDateError'));
    var securityCodeValid = validateSecurityCode($j('#security_code').val(), $j('#securityCodeError'));
    return (nameValid && phoneValid && streetValid && cityValid &&
            postcodeValid && cardholderNameValid && cardNumberValid && expiryMonthValid && expiryYearValid && securityCodeValid);
}

function validateOrderForm() {
    return true;
}

// Function to validate the login form
function validateEmailLogin(input, errorDisplayId) {
    if (validateNotEmpty(input, errorDisplayId)) {
        console.log("Email not empty");
        var xmlhttp;
        if (validateEmailFormat(input)) {
            console.log("Email format is valid");
            try {
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                        var result = xmlhttp.responseText;
                        console.log("Email query result is: " + result);
                        if (result === 1) {
                            $j(errorDisplayId).text("Email verified");
                        } else {
                            $j(errorDisplayId).text("Email not recongnised. Did you mean to register?");
                        }
                    }
                };
                xmlhttp.open("GET", "check-email.php?q=" + input, true);
                xmlhttp.send();
            } catch (exception) {
                alert('Request Failed');
            }
        } else {
            $j("#emailError").text("Email format is invalid");
        }
    }
}