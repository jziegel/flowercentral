function showItem(str) {
	console.log("showItem called" );
	$j("#dropdown").addClass("show");
	if (str=="") {
		$j("#dropdown").html("<p>No result</p>");
		return;
	} 
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			$j("#dropdown").html(xmlhttp.responseText);
		}
	}
	xmlhttp.open("GET","search.php?q="+str,true);
	xmlhttp.send();
}

window.onclick = function(event) {
	if (!event.target.matches('#submit')) {
		var dropdowns = document.getElementsByClassName("dropdown-content");
		var i;
		for (i = 0; i < dropdowns.length; i++) {
	  		var openDropdown = dropdowns[i];
	  		if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
	  		}
		}
  	}
}