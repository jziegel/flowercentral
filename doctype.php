<?php 
session_start(); 

$check = @md5(
        $_SERVER['HTTP_ACCEPT_CHARSET'] . 
        $_SERVER['HTTP_ACCEPT_ENCODING'] .
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] .
        $_SERVER['HTTP_USER_AGENT']);                                 

if (isset($_SESSION['key'])) {
    if (empty($_SESSION)) {
            $_SESSION['key'] = $check;
    } else if ($_SESSION['key'] != $check){
            session_destroy();
    }
}

ini_set("display_errors", FALSE);
ini_set("log_errors", TRUE);
ini_set("error_log", "var/log/fc_php.log");

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">';

?>