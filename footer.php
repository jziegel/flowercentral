<div class="footer">
    <div class="left_footer"><img src="images/footer_logo.gif" alt="" title="" /><br />
        <a href="http://csscreme.com/freecsstemplates/" title="free templates"><img src="images/csscreme.gif" alt="free templates" title="free templates" border="0" /></a></div>
    <div class="right_footer"> <a href="index.php">Home</a> <a href="about.php">About</a> <a href="plants.php">Plants</a> <a href="gifts.php">Gifts</a> <a href="contact.php">Contact</a> </div>
</div>
<div class="disclaimer">
    <p>&copy; Deakin University, School of Information Technology. This web page has been developed as a student assignment for the unit SIT203: Web Programming. Therefore it is not part of the University's authorised web site. DO NOT USE THE INFORMATION CONTAINED ON THIS WEB PAGE IN ANY WAY.</p>
</div>