<?php

$q = $_GET["q"];

require_once('inc/global-connect.inc.php');

// Find new products
$query = "SELECT ID, new
		 FROM Plants
		 WHERE new LIKE 't'
		 UNION
		 SELECT ID, new
		 FROM Gifts
		 WHERE new like 't'";

// Run query
$stmt = oci_parse($connect, $query);
if (!$stmt) {
    echo "An error occurred in parsing the sql string.\n";
    exit;
}
oci_execute($stmt);

$new_products = array();

while (oci_fetch_array($stmt)) {
    $new_products[] = oci_result($stmt, "ID");
}

$new_count = count($new_products);

$selected_products = array_rand($new_products, $q);

$selected_count = count($selected_products);

for ($x = 0; $x < $selected_count; $x++) {
    $product_type = substr($new_products[$selected_products[$x]], 0, 1);
    $new_query;
    switch ($product_type) {
        case 1:
            //echo "Product is a Plant<br/>";
            $new_query = "SELECT *
							  FROM Plants
							  WHERE ID = '" . $new_products[$selected_products[$x]] . "'";
            $stmt = oci_parse($connect, $new_query);
            if (!$stmt) {
                echo "An error occurred in parsing the sql string.\n";
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                echo '<div class="new_prod_box">
				 			<a href="details.php?id=' . oci_result($stmt, "ID") . '">' . oci_result($stmt, "NAME") . '</a>
                    		<div class="new_prod_bg_blank">
								<span class="new_icon"><img src="images/new_icon.gif" alt="" title="" /></span>
								<a href="details.php?id=' . oci_result($stmt, "ID") . '">
									<img src="images/products/' . oci_result($stmt, "LINK") . '_thumb.jpg" class="thumb" alt="" title="" border="0" />			
								</a>
							</div>
						</div>';
            }
            break;
        case 2:
            $new_query = "SELECT *
							  FROM Gifts
							  WHERE ID = '" . $new_products[$selected_products[$x]] . "'";
            $stmt = oci_parse($connect, $new_query);
            if (!$stmt) {
                echo "An error occurred in parsing the sql string.\n";
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                echo '<div class="new_prod_box">
				 			<a href="details.php?id=' . oci_result($stmt, "ID") . '">' . oci_result($stmt, "NAME") . '</a>
                    		<div class="new_prod_bg_blank">
								<span class="new_icon"><img src="images/new_icon.gif" alt="" title="" /></span>
								<a href="details.php?id=' . oci_result($stmt, "ID") . '">
									<img src="images/products/' . oci_result($stmt, "LINK") . '_thumb.jpg" class="thumb" alt="" title="" border="0" />			
								</a>
							</div>
						</div>';
            }
            break;
    }
}

// Close the connection
oci_close($connect);
?>