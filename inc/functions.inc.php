<?php

// Function to display a count of items in the shopping cart
function writeShoppingCart() {
    $cart = $_SESSION['fc_cart'];
    if (!$cart) {
        return '<p>You have no items in your shopping cart</p>';
    } else {
        // Parse the cart session variable
        $items = explode(',', $cart);
        $s = (count($items) > 1) ? 's' : '';
        return '<p>You have <a href="cart.php">' . count($items) . ' item' . $s . ' in your shopping cart</a></p>';
    }
}

// Function to display the items in the shopping cart in a table format
function showCart() {
    global $connect;

    // Get the cart session
    $cart = $_SESSION['fc_cart'];
    if ($cart) {

        // Go through the items in the cart and count each type
        $items = explode(',', $cart);
        $contents = array();
        foreach ($items as $item) {
            $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
        }
        // Variables for tracking price
        $total = $gst = 0;
        $shipping = 10;

        // Start the table output
        $output[] = '<form action="cart.php?action=update" method="post" id="cart">';
        $output[] = '<table>';

        $output[] = '<table class="cart_table">
                    <tr class="cart_title">
                        <td>Item image</td>
                        <td>Product name</td>
                        <td>Unit price</td>
                        <td>Quantity</td>
                        <td>Total</td>
                        <td>Action</td>
                    </tr>';

        // Create a query based on which tyoe of item is selected
        foreach ($contents as $id => $qty) {
            $product_type = substr($id, 0, 1);
            switch ($product_type) {
                case 1:
                    $sql = 'SELECT *
                            FROM Plants
                            WHERE ID = ' . $id;
                    break;
                case 2:
                    $sql = 'SELECT *
                            FROM Gifts
                            WHERE ID = ' . $id;
                    break;
            }

            // Run the query
            $stmt = oci_parse($connect, $sql);
            if (!$stmt) {
                echo '<p class="details">An error occurred in parsing the sql string.</p>';
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                $name = oci_result($stmt, "NAME");
                $price = oci_result($stmt, "PRICE");
                $link = oci_result($stmt, "LINK");
            }

            // Output the product details
            $output[] = '<tr>
                        <td><a href="details.php?id=' . $id . '"><img src="images/products/' . $link . '_thumb.jpg" alt="" width="50" height="50" class="cart_thumb" title="" border="0" /></a></td>
                        <td>' . $name . ' </td>	
                        <td>AU$ ' . $price . '</td>
                        <td><input type="text" name="qty' . $id . '" value="' . $qty . '" size="3" maxlength="3" /></td>
                        <td>AU$ ' . ($price * $qty) . '</td>
                        <td><a href="cart.php?action=delete&id=' . $id . '" class="r">Remove</a></td>';

            // Update the total price
            $total += $price * $qty;
            $output[] = '</tr>';
        }

        // Format GST to 2 decimal points
        $gst = number_format(($total / 10), 2);

        // Update total
        $total += $gst + $shipping;

        // Output price details
        $output[] = '</table>';
        $output[] = '<table class="cart_table">
                    	<tr>
                            <td><p class="total">GST: AU$ ' . $gst . '</p></td>
                        </tr>
                        <tr>
                            <td><p class="total">Shipping: AU$ ' . $shipping . '</p></td>
                        </tr>
                        <tr class="cart_title">
                            <td><p class="total">Grand total: <strong>AU$ ' . $total . '</strong></p></td>
                        </tr>
                     </table>';
        $output[] = '<div><button type="submit" class="button update_cart_button">Update cart</button></div>';
        $output[] = '</form>';
        $output[] = '<button onclick="window.location.href=\'order.php?checkout\'" class="button checkout_cart_button">Checkout &gt;</button>';
    } else {
        $output[] = '<p class"details">Your shopping cart is empty.</p>';
    }

    // Return the complete output
    return join('', $output);
}

// Function to display mini-cart on top right of page
function showMyCart() {
    global $connect;

    // Get the cart session
    $cart = $_SESSION['fc_cart'];

    if ($cart) {
        // Go through the items in the cart and count each type		
        $items = explode(',', $cart);
        $itemCount = count($items);
        $contents = array();
        foreach ($items as $item) {
            $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
        }

        // Variables for tracking price
        $total = $gst = 0;
        $shipping = 10;

        // Create a query based on the product type
        foreach ($contents as $id => $qty) {
            $product_type = substr($id, 0, 1);
            switch ($product_type) {
                case 1:
                    $sql = 'SELECT *
                            FROM Plants
                            WHERE ID = ' . $id;
                    break;
                case 2:
                    $sql = 'SELECT *
                            FROM Gifts
                            WHERE ID = ' . $id;
                    break;
            }

            // Run the query
            $stmt = oci_parse($connect, $sql);
            if (!$stmt) {
                echo '<p class="details">An error occurred in parsing the sql string.<p>';
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                $price = oci_result($stmt, "PRICE");
            }

            // Update price
            $total += $price * $qty;
        }
        // Calculate gst, shipping and update price
        $gst = number_format(($total / 10), 2);
        $total += $gst + $shipping;

        // Output details
        echo '<div class="home_cart_content">' . $itemCount . ' x items | <span class="red">TOTAL: AU$ ' . $total . '</span> </div>
                <button onclick="window.location.href=\'cart.php\'" class="button view_cart_button">View cart</button>
            </div>';
    } else {
        echo '<div class="home_cart_content">No items in cart yet</div></div>';
    }
}

// Function to display items that have been checked out
function showCheckoutItems() {
    global $connect;

    // Get the cart session
    $cart = $_SESSION['fc_cart'];

    if ($cart) {

        // Go through the items in the cart and count each type		
        $items = explode(',', $cart);
        $itemCount = count($items);
        $contents = array();
        foreach ($items as $item) {
            $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
        }

        // Variables for tracking price
        $total = $gst = 0;
        $shipping = 10;

        // Create a query based on the product type
        foreach ($contents as $id => $qty) {
            $product_type = substr($id, 0, 1);
            switch ($product_type) {
                case 1:
                    $sql = 'SELECT *
                            FROM Plants
                            WHERE ID = ' . $id;
                    break;
                case 2:
                    $sql = 'SELECT *
                            FROM Gifts
                            WHERE ID = ' . $id;
                    break;
            }

            // Run the query	
            $stmt = oci_parse($connect, $sql);
            if (!$stmt) {
                echo '<p class="details">An error occurred in parsing the sql string.</p>';
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                $name = oci_result($stmt, "NAME");
                $price = oci_result($stmt, "PRICE");
                $output[] = '<li>' . $name . ' </li>';
            }

            // Update price
            $total += $price * $qty;
        }
        // Calculate gst, shipping and update price
        $gst = number_format(($total / 10), 2);
        $total += $gst + $shipping;

        // Output details
        $output[] = '<strong>TOTAL AU$ ' . $total . ' </strong>';
    } else {
        $output[] = '<li">No items in cart yet</li>';
    }
    return join('', $output);
}

// Function to remove unwanted characters from a given string
function test_input($data) {
    // Test the data if it isn't null
    if ($data != null) {
        $data = trim($data);
        $data = strip_tags($data);
        $data = stripslashes($data);
        $data = htmlentities($data, ENT_QUOTES);
    // If data is null, give it a blank value
    } else {
        $data = "";
    }
    return $data;
}

// Function to process cart and do checkout
function processCart() {
    global $connect;

    // Get the cart session
    $cart = $_SESSION['fc_cart'];

    if ($cart) {
        // Start output
        $output[] = '<div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Order details</div>
                            <div class="feat_prod_box_details">
                            <p class="details">Please note the below order details for your records.<br/>
                            The details will be available on your account for future reference.</p>
                    </div>';

        $output[] = '<table class="order_table">';

        // Go through the items in the cart and count each type	
        $items = explode(',', $cart);
        $itemCount = count($items);
        $contents = array();
        foreach ($items as $item) {
            $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
        }
        $total = $gst = 0;
        $shipping = 10;

        // Create a query based on the product type
        foreach ($contents as $id => $qty) {
            $product_type = substr($id, 0, 1);
            switch ($product_type) {
                case 1:
                    $query = '  SELECT *
                                FROM Plants
                                WHERE ID = ' . $id;
                    break;
                case 2:
                    $query = '  SELECT *
                                FROM Gifts
                                WHERE ID = ' . $id;
                    break;
            }

            // Run the query	
            $statement = oci_parse($connect, $query);
            if (!$statement) {
                echo '<p class="details">An error occurred in parsing the sql string.</p>';
                exit;
            }
            oci_execute($statement);

            // Keeo a count of utem for display purposes
            $count = 1;
            while (oci_fetch_array($statement)) {
                $name = oci_result($statement, "NAME");
                $price = oci_result($statement, "PRICE");
                $output[] = '<tr>
                                <td>(' . $count . ') ' . $name . '</td>
                                <td>AU$ ' . $price . '</td>
                            </tr>';
                $count++;
            }

            // Update total
            $total += $price * $qty;
        }
        // Update and display subtotal
        $output[] = '<tr>
                        <td>Subtotal:</td>
                        <td>AU$ ' . $total . '</td>
                    </tr>';

        // Update and display gst, shipping
        $gst = number_format(($total / 10), 2);
        $output[] = '<tr>
                            <td>GST:</td>
                            <td>AU$ ' . $gst . '</td>
                    </tr>';
        $output[] = '<tr>
                        <td>Shipping:</td>
                        <td>AU$ ' . $shipping . '</td>
                    </tr>';

        // Update and display total
        $total += $gst + $shipping;
        $output[] = '<tr>
                        <td><strong>Total charged:</strong></td>
                        <td>AU$ ' . $total . '</td>
                    </tr>';

        // Start processing irder query
        $email = $_SESSION['fc_useremail'];

        // First create an order and add the order details to the Order table
        $orderQuery = " INSERT INTO Orders(id, user_email, order_total)
                        VALUES (orders_seq.NEXTVAL, '$email', '$total')";

        // Run query
        $statement = oci_parse($connect, $orderQuery);
        if (!$statement) {
            echo '<p class="details">An error occurred in parsing the sql string.</p>';
            exit;
        }
        oci_execute($statement);
        oci_commit($connect);

        // Iterate through each item and insert it into the Order Details table
        foreach ($contents as $id => $qty) {

            // Query
            $orderDetailsQuery = "INSERT INTO Order_Details
				  VALUES (order_details_seq.NEXTVAL, orders_seq.CURRVAL, '$id')";

            // Run query
            $statement = oci_parse($connect, $orderDetailsQuery);
            if (!$statement) {
                echo '<p class="details">An error occurred in parsing the sql.</p>';
                exit;
            }
            oci_execute($statement);
            oci_commit($connect);

            // Query the stock count for the product id
            $stockBalanceQuery = "  SELECT quantity
                                    FROM Stock
                                    WHERE product_id = '$id'";

            // Run query
            $statement = oci_parse($connect, $stockBalanceQuery);
            if (!$statement) {
                echo "An error occurred in parsing the sql string.";
                exit;
            }
            oci_execute($statement);

            // Get the current stock balance
            $stockBalance = 0;
            while (oci_fetch_array($statement)) {
                $stockBalance = oci_result($statement, "QUANTITY");
            }

            // Update the stock count
            $stockBalance -= 1;
            $stockUpdateQuery = "   UPDATE Stock
                                    SET quantity = '$stockBalance'
                                    WHERE product_id = '$id'";

            // Run query
            $statement = oci_parse($connect, $stockUpdateQuery);
            if (!$statement) {
                echo '<p class="details">An error occurred in parsing the sql string.</p>';
                exit;
            }
            oci_execute($statement);
            oci_commit($connect);
        }

        // Close table
        $output[] = '</table>';

        // Clear cart
        $_SESSION['fc_cart'] = '';

        // Return complete output
        return join('', $output);
    } else {
        echo '<p class="details">No items in cart yet</p>';
    }
}

// Function to display the user's completed orders
function showMyOrders() {
    global $connect;

    // Get the username
    if (isset($_SESSION['fc_useremail'])) {
        $email = $_SESSION['fc_useremail'];

        // Use the user email to get the list of the user's orders 
        $orderQuery = " SELECT o.id, o.order_date, o.user_email, o.order_total, od.item_id
                        FROM orders o, order_details od
                        WHERE o.id = od.order_id
                        ORDER BY o.id DESC";

        $order_id = $order_date = $order_total = $item_id = $name = $link = "";

        // Run query
        $statement = oci_parse($connect, $orderQuery);
        if (!$statement) {
            echo "An error occurred in parsing the sql string.";
            exit;
        }
        oci_execute($statement);

        // Start table output
        $output[] = '<table class="cart_table">
                    <tr class="cart_title">
                        <td>Order ID</td>
                        <td>Date</td>
                        <td>Product name</td>
                        <td>Product image</td>
                        <td>Invoice total</td>
                    </tr>';

        // Iterate through results array
        while (oci_fetch_array($statement)) {
            $order_id = oci_result($statement, "ID");
            $order_date = oci_result($statement, "ORDER_DATE");
            $order_total = oci_result($statement, "ORDER_TOTAL");
            $item_id = oci_result($statement, "ITEM_ID");

            // Create a qery depending on the product type
            $product_type = substr($item_id, 0, 1);
            switch ($product_type) {
                case 1:
                    $productQuery = 'SELECT *
                                    FROM Plants
                                    WHERE ID = ' . $item_id;
                    break;
                case 2:
                    $productQuery = 'SELECT *
                                    FROM Gifts
                                    WHERE ID = ' . $item_id;
                    break;
            }

            // Run query
            $statementProduct = oci_parse($connect, $productQuery);
            if (!$statementProduct) {
                echo '<p class="details">An error occurred in parsing the sql string.</p>';
                exit;
            }
            oci_execute($statementProduct);

            // Iterate through the results array
            while (oci_fetch_array($statementProduct)) {
                $name = oci_result($statementProduct, "NAME");
                $link = oci_result($statementProduct, "LINK");
                ;
            }

            // Output details
            $output[] = '<tr>
                                <td>' . $order_id . ' </td>
                                <td>' . $order_date . ' </td>
                                <td>' . $name . ' </td>	
                                <td><a href="details.php?id=' . $item_id . '"><img src="images/products/' . $link . '_thumb.jpg" alt="" width="50" height="50" class="cart_thumb" title="" border="0" /></a></td>
                                <td>AU$ ' . $order_total . '</td>
                        </tr>';
        }
        // Close table
        $output[] = '</table>';

        // Return completed output
        return join('', $output);
    }
}

?>
