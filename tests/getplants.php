<?php

/*
The database table we use in this example looks like this:
id FirstName LastName Age Hometown Job
1 Mary Doss 42 Geelong High school teacher
2 Robert Dew 40 Newport Piano Teacher
3 Justin Rough 34 Geelong University Lecturer
*/

$q=$_GET["q"]; //or $q=$_REQUEST["q"];

/* Set oracle user login and password info */
$dbuser = "jziegel"; /* your deakin login */
$dbpass = "jerome3150"; /* your oracle access password */
$db = "localhost/pdborcl";
$connect = oci_connect($dbuser, $dbpass, $db);

if (!$connect) {
	echo "An error occurred connecting to the database";
	exit;
}

/* build sql statement using form data */
$query ="SELECT * FROM Plants WHERE UPPER(Name) LIKE UPPER('".$q."%')";

/* check the sql statement for errors and if errors report them */
$stmt = oci_parse($connect, $query);

if(!$stmt) {
	echo "An error occurred in parsing the sql string.\n";
	exit;
}
oci_execute($stmt);

echo "<table border='1'>
<tr>
<th>Name</th>
</tr>";

// Display all the values in the retrieved records, one record per row, in a loop
while(oci_fetch_array($stmt)) {
	echo "<tr>";
	echo "<td>" . oci_result($stmt,"NAME") . "</td>";
	echo "</tr>";
}
echo "</table>";

// Close the connection
oci_close($connect); 
?>