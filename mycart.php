<?php
require('inc/global-connect.inc.php');
require('inc/functions.inc.php');

// I f the session or cookie is set, that means the user is logged in
if (isset($_SESSION['fc_username']) || isset($_COOKIE["fc_username"])) {
    echo '<div class="cart">
                    <div class="title"><span class="title_icon"><img src="images/cart.gif" alt="" title="" /></span>My cart</div>';
    echo showMyCart();
} else {
    echo '<p><a href="myaccount.php">Log in</a> to view your cart.</p>';
}
?>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    						  *
***************************************************************************************
-->