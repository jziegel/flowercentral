<?php include 'doctype.php' ?>

<head>

<?php    
include 'meta.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/sit203/securimage/securimage.php';
?>
<title>Register | Flower Shop</title>
</head>

<body>

    <script type="text/javascript">
        // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highglight menu
            $j("#menu-contact").addClass('selected');
        });
    </script>

    <div id="wrap">

        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Full content -->
        <div class="full_page">
            <div class="full_page_container">

                <?php
                
                // Process a post request of there is one
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    
                    // Get a connection to the database and load common functions
                    require_once('inc/global-connect.inc.php');
                    require_once('inc/functions.inc.php');
                    
                    // A new Securimage to check the value sent through post
                    $securimage = new Securimage();
                    
                    // If the security code is not correct, display an error message and end the page
                    if ($securimage->check($_POST['captcha_code']) == false) {
                        echo '  <div class="title">
                                    <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
                                </div>
                                <div class="feat_prod_box_details">
                                    <p class="details">The security code entered was incorrect.Please go <a href=\'javascript:history.go(-1)\'>back</a> and try again.</p>
                                </div>
                                <div class="clear"></div>
                                </div>';
                        include 'footer.php';
                        die( '  </div>
                                </body>
                                </html>');
                    }
                    $name = test_input($_POST['name']);
                    $email = test_input($_POST['email']);
                    $phone = test_input($_POST['phone']);
                    $company = test_input($_POST['company']);
                    $message = test_input($_POST['message']);
                                        
                    // Create a query to save the message in the database
                    $contactQuery = "   INSERT INTO Contact(id, name, email, phone, company, message)
                                        VALUES (contact_seq.NEXTVAL, '$name', '$email', '$phone', '$company', '$message')";

                    // Run query
                    $statement = oci_parse($connect, $contactQuery);
                    
                    // Show an error message and clean up if there is an error with the statement
                    if (!$statement) {
                        echo '  <div class="title">
                                    <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
			  	</div>
                                <div class="feat_prod_box_details">
                                    <p class="details">Error in preparing query statement. Please go back and try again.</p>
                                </div>';
                        include 'footer.php';
                        die( '  </div>
                                </body>
                                </html>');
                    }
                    
                    // Execute the statememt, commit and close the connection
                    oci_execute($statement);
                    oci_commit($connect);
                    oci_close($connect); 
                    
                    // Notify the user that the message has been received
                    echo '  <div class="title">
                                <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Thank you.
                            </div>
                            <div class="feat_prod_box_details">
      				<p class="details">The following message has been received, we will be in touch with you shortly.</p>
                                    <ul>
                                        <li>Name: ' . $name .'</li>
                                        <li>Email: ' . $email .'</li>
                                        <li>Phone: ' . $phone .'</li>
                                        <li>Company: ' . $company .'</li>
                                        <li>Message: ' . $message .'</li>
                                    </ul>
                            </div>
                            <div class="clear"></div>
                            </div>';
                    
                // If there is no post request, display an error message
                } else {
                    echo '  <div class="title">
                                <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
                            </div>
                            <div class="feat_prod_box_details">
      				<p class="details">Please visit the <a href=\'contact.php\'>Contact</a> page to contact us.</p>
                            </div>
                            <div class="clear"></div>
                            </div>';
                    include 'footer.php';
                    die( '  </div>
                            </body>
                            </html>');
                }
                ?>
                <div class="clear"></div>
            </div>
            <!-- End full content--> 

            <!-- Footer -->
            <?php include 'footer.php'; ?>
            <!-- End Footer --> 
        </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                                              *
***************************************************************************************
-->