<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Register | Flower Shop</title>
</head>

<body>

    <script type="text/javascript">
    // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highglight menu
            $j("#menu-register").addClass('selected');
        });
    </script>

    <div id="wrap">

        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header --> 

        <!-- Full content -->
        <div class="full_page">
            <div class="full_page_container">

                <?php
// Start completing order if post is detected
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    require_once('inc/global-connect.inc.php');
                    require_once('inc/functions.inc.php');

                    $name = $phone = $unit_no = $street = $city = $postcode = $cardholder_name = $card_type = $card_number = $expiry_month = $expiry_year = "";

                    // Get values
                    $name = test_input($_POST['name']);
                    $phone = test_input($_POST['phone']);
                    $unit_no = test_input($_POST['unit_no']);
                    $street = test_input($_POST['street']);
                    $city = test_input($_POST['city']);
                    $postcode = test_input($_POST['postcode']);
                    $cardholder_name = test_input($_POST['cardholder_name']);
                    $card_type = test_input($_POST['card_type']);
                    $card_number = test_input($_POST['card_number']);
                    $expiry_month = test_input($_POST['expiry_month']);
                    $expiry_year = test_input($_POST['expiry_year']);

                    // Get cart and email
                    $cart = $_SESSION['fc_cart'];
                    $email = $_SESSION['fc_useremail'];

                    // Create an update query to update the user'd details
                    $update_query = "UPDATE Users
					SET name ='$name', phone='$phone', unit_no='$unit_no', street='$street',
						city='$city', postcode='$postcode', cardholder_name='$cardholder_name',
						card_type='$card_type', card_number='$card_number',
						expiry_month='$expiry_month', expiry_year='$expiry_year'
					WHERE EMAIL='$email'";

                    // Run query
                    $statement = oci_parse($connect, $update_query);
                    if (!$statement) {
                        echo '<div class="title">
					<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
			  	</div>
    			<div class="feat_prod_box_details">
      				<p class="details">Error in preparing query statement. Please go back and try again.</p>
    			</div>';
                        exit;
                    }
                    oci_execute($statement);

                    // Process cart
                    echo processCart();

                    oci_commit($connect);
                    oci_close($connect);
                }
                ?>
                <div class="clear"></div>
            </div>
            <!-- End full content--> 

            <!-- Footer -->
<?php include 'footer.php'; ?>
            <!-- End Footer --> 
        </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                    			      *
***************************************************************************************
-->