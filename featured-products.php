<?php
$q = $_GET["q"];

require_once('inc/global-connect.inc.php');

// Find featured products
$query = "SELECT ID, featured
		 FROM Plants
		 WHERE featured LIKE 't'
		 UNION
		 SELECT ID, featured
		 FROM Gifts
		 WHERE featured like 't'";

// Check statement for errors
$stmt = oci_parse($connect, $query);

// Display an error if there is one
if (!$stmt) {
    echo '<p class="details>An error occurred in parsing the sql string.</p>';
    exit;
}

oci_execute($stmt);

$featured_products = array();

while (oci_fetch_array($stmt)) {
    $featured_products[] = oci_result($stmt, "ID");
}

// Get a count of the featured products
$featured_count = count($featured_products);

// Choose randomly from the array
$selected_products = array_rand($featured_products, $q);

// Count the selected products
$selected_count = count($selected_products);

// Iterate through the selected products and display details
for ($x = 0; $x < $selected_count; $x++) {

    $product_type = substr($featured_products[$selected_products[$x]], 0, 1);
    $featured_query;
    switch ($product_type) {
        case 1:
            // Product is a plant
            $featured_query = "SELECT *
							  FROM Plants
							  WHERE ID = '" . $featured_products[$selected_products[$x]] . "'";
            $stmt = oci_parse($connect, $featured_query);
            if (!$stmt) {
                echo "An error occurred in parsing the sql string.\n";
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                echo '<div class="feat_prod_box">
							<div class="prod_img">
								<a href="details.php?id=' . oci_result($stmt, "ID") . '">
									<img src="images/products/' . oci_result($stmt, "LINK") . '.jpg" alt="" title="" border="0" />
								</a>
							</div>
							<div class="prod_det_box">
								<div class="box_top"></div>
								<div class="box_center">
									<div class="prod_title">' . oci_result($stmt, "NAME") . '</div>
									<p class="details">' . oci_result($stmt, "DESCRIPTION") . '</p>
									<a href="details.php?id=' . oci_result($stmt, "ID") . '" class="more">- more details -</a>
									<div class="clear"></div>
								</div>
								<div class="box_bottom"></div>
							</div>
							<div class="clear"></div>
					</div>';
            }
            break;
        case 2:
            // Product is a Gift
            $featured_query = "SELECT *
							  FROM Gifts
							  WHERE ID = '" . $featured_products[$selected_products[$x]] . "'";
            $stmt = oci_parse($connect, $featured_query);
            if (!$stmt) {
                echo "An error occurred in parsing the sql string.\n";
                exit;
            }
            oci_execute($stmt);
            while (oci_fetch_array($stmt)) {
                echo '<div class="feat_prod_box">
							<div class="prod_img">
								<a href="details.php?id=' . oci_result($stmt, "ID") . '">
									<img src="images/products/' . oci_result($stmt, "LINK") . '.jpg" alt="" title="" border="0" />
								</a>
							</div>
							<div class="prod_det_box">
								<div class="box_top"></div>
								<div class="box_center">
									<div class="prod_title">' . oci_result($stmt, "NAME") . '</div>
									<p class="details">' . oci_result($stmt, "DESCRIPTION") . '</p>
									<a href="details.php?id=' . oci_result($stmt, "ID") . '" class="more">- more details -</a>
									<div class="clear"></div>
								</div>
								<div class="box_bottom"></div>
							</div>
							<div class="clear"></div>
					</div>';
            }
            break;
    }
}

// Close the connection
oci_close($connect);
?>
</div>