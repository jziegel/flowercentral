<?php include 'doctype.php'; ?>
<head>
    <?php include 'meta.php'; ?>
    <title>Flowers | Flower Shop</title>
</head>
<body>

    <script type="text/javascript">
    // Start jQuery in no-conflict mode
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            // Highlight menu
            $j("#menu-register").addClass('selected');
        });
    </script> 

    <div id="wrap">
        <!-- Header -->
        <?php include 'header.php'; ?>
        <!-- End header -->

        <div class="full_page">
            <div class="full_page_container">

                <?php
                require_once('inc/global-connect.inc.php');
                require_once('inc/functions.inc.php');

                $cart = $_SESSION['fc_cart'];
                $email = $_SESSION['fc_useremail'];


                // Query to find a matching user
                $query = "SELECT *
                  FROM Users
                  WHERE email = '$email'";

                $stmt = oci_parse($connect, $query);

                // Display an error if there is a problem with the statement
                if (!$stmt) {
                    echo '<div class="title">
                    <span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Error
                  </div>
                  <div class="feat_prod_box_details">
                      <p class="details">Error in preparing query statement. Please go back and try again.</p>
                  </div>';
                    exit;
                }

                // Execute the statement
                oci_execute($stmt);

                $name = $phone = $unitno = $street = $city = $postcode = $cardholder_name = $card_type = $card_number = $expiry_month = $expiry_year = "";

                while (oci_fetch_array($stmt)) {
                    // Get the user details
                    $name = oci_result($stmt, "NAME");
                    $phone = oci_result($stmt, "PHONE");
                    $unit_no = oci_result($stmt, "UNIT_NO");
                    $street = oci_result($stmt, "STREET");
                    $city = oci_result($stmt, "CITY");
                    $postcode = oci_result($stmt, "POSTCODE");
                    $cardholder_name = oci_result($stmt, "CARDHOLDER_NAME");
                    $card_type = oci_result($stmt, "CARD_TYPE");
                    $card_number = oci_result($stmt, "CARD_NUMBER");
                    $expiry_month = oci_result($stmt, "EXPIRY_MONTH");
                    $expiry_year = oci_result($stmt, "EXPIRY_YEAR");
                }

                // Display checkout form if the get variable is set
                if (isset($_GET['checkout'])) {
                    echo '<div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Complete order</div>
                    <div class="order_intro">
                        <p class="details">Please fill in your contact details and delivery address, and proceed with your order.</p>
                    </div>';

                    echo '<form class="user_form" action="complete-order.php" onsubmit="return validateOrderForm();" method="post" >
                        <div class="row">
                            <label class="user_form_title"><strong>Selected items:</strong></label>
                            <div class="user_form_items">
                                <ul>'
                    . showCheckoutItems() .
                    '</ul>
                            </div>
                        </div>                        
                        <div class="row">
                            <label class="user_form_title"><strong>Name:*</strong></label>
                            <input type="text" name="name" id="name" autofocus="autofocus" class="user_form_info" value="' . $name . '"
                              onblur = "validateNotEmpty(this.value, \'#nameError\')"/>
                            <span id = "nameError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Phone number:*</strong></label>
                            <input type="text" name="phone" id="phone" class="user_form_info" value="' . $phone . '"
                                  onblur = "validatePhone(this.value, \'#phoneError\')"/>
                            <span id = "phoneError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Unit no:</strong></label>
                            <input type="text" name="unit_no" id="unit_no" class="user_form_info_short" value="' . $unit_no . '"/>
                            <label class="user_form_title_short"><strong>Street:*</strong></label>
                            <input type="text" name="street" id="street" class="user_form_info_short" value="' . $street . '"
                                onblur = "validateNotEmpty(this.value, \'#streetError\')"/>
                            <span id = "streetError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>City:</strong></label>
                            <input type="text" name="city" id="city" class="user_form_info" value="' . $city . '"
                                onblur = "validateNotEmpty(this.value, \'#cityError\')"/>
                            <span id = "cityError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Post code:</strong></label>
                            <input type="text" name="postcode" id="postcode" class="user_form_info" value="' . $postcode . '"
                              onblur = "validatePostCode(this.value, \'#postCodeError\')"/>
                            <span id = "postCodeError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Cardholders name:*</strong></label>
                            <input type="text" name="cardholder_name" id="cardholder_name" class="user_form_info" value="' . $cardholder_name . '"
                                onblur = "validateNotEmpty(this.value, \'#cardholderNameError\')"/>
                            <span id = "cardholderNameError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Card type:*</strong></label>
                            <select name="card_type">                           
                                <option value="visa"' . (($card_type == 'visa') ? 'selected="selected"' : "") . '>VISA</option>
                                <option value="mastercard" ' . (($card_type == 'mastercard') ? 'selected="selected"' : "") . '>Master Card</option>
                                <option value="americanexpress" ' . (($card_type == 'americanexpress') ? 'selected="selected"' : "") . '>American Express</option>
                            </select>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Card number:*</strong></label>
                            <input type="text" name="card_number" id="card_number" class="user_form_info" value="' . $card_number . '"
                              onblur = "validateCardNumber(this.value, \'#cardNumberError\')"/>
                            <span id = "cardNumberError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>Expiry:*</strong></label>
                            <div class="expriy">
                                <input type="text" name="expiry_month" id="expiry_month" value="' . $expiry_month . '"
                                  onblur = "validateExpiryDate(this.value, \'#expiryDateError\')"/>
                                /
                                <input type="text" name="expiry_year" id="expiry_year" value="' . $expiry_year . '"
                                  onblur = "validateExpiryDate(this.value, \'#expiryDateError\')"/>
                            </div>
                            <span id = "expiryDateError" class = "error"></span>
                        </div>
                        <div class="row">
                            <label class="user_form_title"><strong>CVV:*</strong></label>
                            <div class="expriy">
                                <input type="text" name="security_code" id="security_code"
                                  onblur = "validateSecurityCode(this.value, \'#securityCodeError\')"/>
                            </div>
                            <span id = "securityCodeError" class = "error"></span>
                        </div>
                        <div class="row">
                            <input type="submit" name="submit_order" id="submit_order" value="Process payment"/>
                        </div>
                        <div class="row">
                            <p class="details">* Denotes mandatory field</p>
                        </div>
                        <div class="clear"></div>
                    </form>';
                } else {
                    echo '<div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>No order</div>
                  <div class="order_intro">
                      <p class="details">Please add items to your cart.</p>
                  </div>
                  </div>';
                }

                oci_commit($connect);
                oci_close($connect);
                ?>

                <div class="clear"></div>
            </div>       

            <!-- End center content-->

            <!-- Footer --> 
            <?php include 'footer.php'; ?>
            <!-- End Footer -->
        </div>
</body>
</html>

<!--
***************************************************************************************
* (C) Deakin University, School of Information Technology. This web page has been     *
* developed as a student assignment for the unit SIT203: Web Programming. Therefore   *
* it is not part of the University's authorised web site. DO NOT USE THE INFORMATION  *
* CONTAINED ON THIS WEB PAGE IN ANY WAY.                                              *
***************************************************************************************
-->